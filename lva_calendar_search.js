$(function () {
	
	$.datepicker.setDefaults( $.datepicker.regional["fr"] );
	
	$('input[data-type=date]').datepicker({
				minDate : 0,
				maxDate : '+1y',
				beforeShow: function(i) { if ($(i).attr('readonly')) { return false; } }
			}).keyup(function(e) {
    if(e.keyCode == 8 || e.keyCode == 46) {
        $.datepicker._clearDate(this);
    }
			});
			
	$('#pays').change(function(){
			if (this.value != 'FR') {
			 	$('#region').hide();
			  	$('#dept').hide();
			}
			else {
				$('#region').show();
			  	$('#dept').show();
			}
		}
	);
            
});