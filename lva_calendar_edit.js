	$(function () {
				var today = new Date();
				var year = today.getFullYear();
				daynames = new Array('samedi', 'dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi');
				daynames_short = new Array('SA', 'SU', 'MO', 'TU', 'WE', 'TH', 'FR');
				
				//décalé par rapport à console.log($("#date_"+ p +"_debut").datepicker( "option", "dayNames" ));
                can_save = true;
				can_submit = false;
				dates_statuts = '';
				dates_supprimees = '';
				date_intervalle = $('#resume_date_txt').val();
				date_ouverture = $('#ouverture_txt').val();
				date_sauf = '';
				resume_date = date_ouverture + ' ' + date_intervalle ;
				$("#resume_date").val(resume_date);
				
				can_generate = true;
				
				
				
				fieldFilter($('#periodicite_manifestation_bak').val(), false, true);
				
				//désactiver la touche entrée p(validation trop rapide du formulaire)
				
				  $(window).keydown(function(event){
					if( (event.keyCode == 13)) {
					  event.preventDefault();
					  return false;
					}
				  });
				  
				
				
				
             
				// Assignation statique des règles de validation
				
				$("#form_manif").validate({ 
					submitHandler: function(form) {
						if (date_validate()) {
							if (can_submit ) {
								$('#form_submit').hide();
								$('#imgLoader').show();
								form.submit();
							}
							else simpledialog('Au minimum un changement est nécessaire pour enregistrer');
						}
						
					 }, 
					onkeyup: false,
				 	onfocusout: function(element, event) {
           			 this.element(element);
       				 },
					 rules: {
						email1: "email",
						email2: "email",
						email_organisateur: "email",
						telephone: "mvs",
						fax: "mvs",
						pays: "required",
						pays_orga:"required",
						id_categorie: "required",
						cp: {
						  required: {
							depends: function(element) {
							  return $("#pays option:selected").val()
							}
						  }
						},
						cp_orga: {
						  required: {
							depends: function(element) {
							  return $("#pays_orga option:selected").val()
							}
						  }
						},
						ville_orga: {
						  required: {
							depends: function(element) {
							  return $("#cp_orga").val()
							}
						  }
						},
						nb_exposant: {
							max:100000,
							digits:true
						},
						tarif: "prix"
						
						
					},
				messages: {
					
					
					
				}
			});
			
			// Traduction française des messages d'erreur
			$.extend($.validator.messages, {
				
				email: "Veuillez entrer une adresse email valide (Ex. nom.prenom@domaine.com)",
				required: "Ce champ est obligatoire",
				tel: "Le téléphone doit contenir uniquement des chiffres, espaces blancs ou tirets (minimum 6 caractères)",
				comment: "Si vous n'indiquez pas de téléphone, le commentaire ne peut être ",
				date:"La date doit être de la forme jj/mm/aaaa (Ex. 01/09/2015) et ne pas excéder " + (year +2),
				shorturl:"Veuillez entrer une adresse de site web valide (ex. www.domaine.com)"
				
				
			   
			});
			
			// règles pour gratuit et payant (radio like behaviour)
			$('#gratuite').click( function(value, element) { 
				$('#payante').prop("checked",false);
				if ($(this).is(":checked"))	$('#tarif').val("");
			}); 
			$('#payante').click( function(value, element) { 
				$('#gratuite').prop("checked",false);
			}); 
			
			// Règles dynamiques pour champs multiples sites web :ne doit pas être une adresse email
			$.each($(".siteweb"), 
				function() {
					var mess = $.validator.messages.shorturl;
					
					$(this).rules("add",{
						shorturl: {
						  	
						}
					});
				}
			);
			
			
						
			// Règle dynamique pour champs multiples commentaires téléphones (seulement si le téléphone associé n'est pas renseigné))
			
			$.each($("fieldset.ftel input[type=text]"), 
				function() {
					var elementId =  $(this).attr('id');
					
					var mess = $.validator.messages.comment;
					
					$(this).rules("add",{
						comment: {
									depends: function() {
								  		return !$("#"+elementId).prev().val()
									}
						  	
						},
						messages : {
							comment:  function() {
									if (/_orga$/.test(elementId)) 	return mess + "renseigné";
									else return mess + "que &laquo; complet &raquo;";
								}
						}
					});
				}
			);
			// Règle pour les champs date
			
			$.each($("input[data-type=date]"), 
				function() {
					var mess = $.validator.messages.date;
					
					$(this).rules("add",{
						date: {
						  	
						}
					});
				}
			);
			
			// Méthodes spécifiques 
			
			// Règle pour le prix d'entrée : nombre à 2 décimales avec séparateur . ou , (inférieur à 1000 euros)
			$.validator.addMethod("prix", function(value, element) {  
        		return this.optional(element) || /^\d{0,3}((,|\.)\d{2})?$/.test(value);
			});  

			// Règle automatiquement appliquée pour tous les champs téléphone de type input[type=tel] : entre  6 et 18 caractères , uniquement  chiffres, tirets - ou espaces blancs
			$.validator.addMethod("tel", function(value, element) { 
					return this.optional(element) || /^[0-9- ]{6,18}$/i.test(value);
			});
			//Règle pour les dates de la forme jj/mm/aaaa
			$.validator.addMethod("date", function(value, element) { 
					pattern = new RegExp(element.pattern,"i");
					return this.optional(element) || pattern.test(value);
			}); 
			 
			// Règle pour les commentaires téléphones : en cas d'absence de téléphone, pour les manifs, le commentaire doit être vide ou "complet", pour les organisateurs, le commentaire doit être vide
			$.validator.addMethod("comment", function(value, element) { 
				
				return (/_orga$/.test(element.id) && value === '') ||  ( !/_orga$/.test(element.id) && (value === 'complet' || value === ''		));
			}); 
			// Règle pour les urls qui ne doivent pas contenir des emails
			$.validator.addMethod("shorturl", function(value, element) { 
				return this.optional(element) ||  /^(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)(:(\d+))?\/?/i.test(value)
				
			}); 
			
			// changement de périodicité : on vide la liste de dates, le résumé et  on filtre les champs à afficher
			$("#periodicite").change(
				function() {
						
					
						var targets = ".bloc_" + $(this).attr('id');
						var targetId =  "#bloc_" + $(this).attr('id') + $(this).val();
						
						$(targets).each(
							function() {
							
								$(this).hide();
							});
						$(targetId).show();
						fieldFilter($(this).val(),1,1);
					
						dlist_populate({},0);
						
					
						
						
				});
			// en cas de choix de la catégorie bourse de jouets, on coche automatiquement jouet
			$("#id_categorie").change(
				function () {
					if  ($(this).val() == 60)	$('#jouet').prop("checked", true);
					else $('#jouet').prop("checked", false);
				}
			);
			// tout changement de date dans le générateur oblige à générer avant de valider les mofifs ( attention !! en cas d'erreur on ne peut plus valider)
			$('#bloc_periodicite2 input, #bloc_periodicite3 input').change(
				function() {
						can_save = false;
						
				});
				
			// changements de date dans la liste générée : on demande  confirmation et on met à jour le champ résumé avec les exceptions	
			$(document).on("change",'#datelist select',
				function () {
					
					can_generate = false;
					dates_statuts = '';
					$.each($("#datelist select"),
						function () {
								if ($(this).val() > 0)	{
								var dates = $(this).parent('fieldset').children("input");
								dates_statuts += ', ' + dates_txt(dates[0].value,dates[1].value,0) + ' est ' + $('#' + $(this).attr('id') + ' option:selected').text(); }
					});
					resume_date = date_ouverture  + ' ' +  date_intervalle + dates_supprimees + dates_statuts ;
					$("#resume_date").val(resume_date); 
		
}
);
			$("#modif_dates").click( // bouton modifier les dates : on affiche les champs de génération, met en place les fene^tres de confirmation de suppression et met  à jour le résumé des dates  avec les valeurs des champs textes
				function() {	
					
					fieldFilter($('#periodicite').val(),1,0);
					
					$("#resume_date").val(resume_date);
		
					$(document).on("click", ".btn-suppr",btn_suppr);
					$('#dates_suppr').on("click",dates_suppr);
						
					
					
				}
			);
			
			// contrôles de cohérence à l'enregistrement des dates
			$("#dates_valid").on("click",function() {
				if (date_validate())	can_submit = true;
			}
			); // bouton valider les dates
				
				
			$("#ponctuel").click( // bouton ajouter date ponctuelle
				function() {
				
						var date = $("#date_debut").val();
						var date2 = $("#date_fin").val();
						var sous_reserve = $('#sous_reserve').is(':checked'); 
						var len = $('.extraDate').length + 1;
						if (addDateFieldset(len,date,date2,sous_reserve,0,0,false)) {
						
					
							$.datepicker._clearDate($('#date_debut'));
							$.datepicker._clearDate($('#date_fin'));
									
							$('#sous_reserve').prop('checked', false);
							if (len <= 1)	$('#datelist > fieldset legend').remove();
							
						}
				}
			);
			
			$("#hebdo, #mensuel").on("click",function () {list_generate($(this))}); //boutons générér dates
			
			
				
			//);
			
			$("#dates_annul").click( //boutons annuler modifications dates : on revient aux valeurs en base chargées par l'API
			
				function() {
					
					var question = "Êtes-vous sûr de vouloir annuler toutes les modifications de dates et revenir aux dates initiales de la manifestation ?";
					confirmation(question,$(this)).then(function (obj) {

						var datas = {};
							datas.dates = JSON.parse($('#date_bak').val());
							datas.resume = $('#resume_date_manifestation_bak').val();
							datas.ouverture = $('#ouverture_manifestation_bak').val();
							$("#periodicite").val($('#periodicite_manifestation_bak').val()).change();	
							var readonly = $('#periodicite_manifestation_bak').val() == 1 ?false:true;
							dlist_populate(datas,readonly);
						
					});
					
				});
				
			//ajustement de la taille du champ textarea en fonction du contenu (on devrait utiliser un div  mais bon...c'est à cause du form generator php qui peuple seulement des éléments de formulaire...)
			if ($("#resume_date_txt").val())	$("#resume_date_txt").prop("rows",parseInt($("#resume_date_txt").val().length/132) + 1);
			
			
			//contrôles sur les les datpeickers
			$.datepicker.setDefaults( $.datepicker.regional["fr"] );	
			$('input[data-type=date]').datepicker({
				minDate : 0,
				maxDate : '31/12/'+(year + 2),
				beforeShow: function(i) { if ($(i).attr('readonly')) { return false; } },
				onSelect:function (date) {
									getDatepickerRange(this.id,date)
									}
			}).keyup(function(e) {
    if(e.keyCode == 8 || e.keyCode == 46) {
        $.datepicker._clearDate(this);
    }
			});
			
function getDatepickerRange(id,date){
					var field = (id.substring(0,2) == 'dd')?'dd':(id.slice(-5) == 'debut'?'debut':(id.slice(-3) == 'fin'?'fin':(id.substring(0,2) == 'df')?'df':''));//console.log(field);
					var linkedfield = (field == 'dd')?'#df':(field == 'df'?'#dd':(field == 'debut'? '#'+id.slice(0,-5):(field == 'fin')? '#'+id.slice(0,-3):''));
					var num = id.substring(2);
					var option = field == 'debut' || field == 'dd' ? 'minDate' : 'maxDate';
					var linkedid = (field == 'dd' || field == 'df' )?linkedfield+num:(field == 'debut'?linkedfield+'fin':(field == 'fin'? linkedfield+'debut':'')); //console.log(date);
					if (linkedid) {
						
						if (!$(linkedid).val())	{
							$(linkedid).datepicker( "setDate", $('#'+id).val());// que si date de fin (option maxDate ?) ou si dates incohérentes
							$(linkedid).datepicker('option',option,date);
					}

					}
					
				}
				
			
// fonction pour ajouter 1 groupe de champs date (date de début - date de fin,  état, suppression)
function addDateFieldset(len,date,date2,sous_reserve,reporte, annule, readonly) //Get the template and update the input field names
{
	if(date != ""){
		if (!date2)	date2 = date;
		if ($.datepicker.parseDate('dd/mm/yy', date2) < today) {
					
					simpledialog('La date de fin est dépassée');
					return false;
		} else if ($.datepicker.parseDate('dd/mm/yy', date2) < $.datepicker.parseDate('dd/mm/yy', date)) {
					
					simpledialog('La date de fin est antérieure à la date de début');
					return false;
		}
		 
	
						
		 $('#datelist > fieldset').append('<fieldset class="extraDate"><label for="dd'+len+'">Du </label><input type="text" data-type="date" name="dd'+len+'" id="dd'+len+'" value="'+date+'" size="10" maxlength="10" placeholder="jj/mm/aaaa" pattern="^([0-2]\\d|3[0-1])\\/(0[1-9]|1[0-2])\\/('+year+'|'+(year+1)+'|'+(year+2)+')$" '+(readonly?'readonly="readonly"':"")+ ' /><label for="df'+len+'"> au </label><input type="text"data-type="date" name="df'+len+'" id="df'+len+'" value="'+date2+'" size="10" maxlength="10" placeholder="jj/mm/aaaa" pattern="^([0-2]\\d|3[0-1])\\/(0[1-9]|1[0-2])\\/('+year+'|'+(year+1)+'|'+(year+2)+')$" '+(readonly?'readonly="readonly"':"")+ ' /><select class="span2" name="status'+len+'" id="status'+len+'" ><option value="">Active</option><option value="1" '+ (annule==1?'selected="selected"':'') +'>Annulée</option><option value="2" '+ (reporte==1?'selected="selected"':'') +'>Reportée</option><option value="4" '+ (sous_reserve==1?'selected="selected"':'') +'>Sous réserve</option></select><button type="button" class="btn btn-default btn-xs btn-suppr" >Supprimer</button></fieldset>');
		
		
		
		 if (!readonly)	$('#dd'+len+',#df'+len).datepicker({
			minDate : 0,
			maxDate : '31/12/'+(year + 2),
			onSelect: function (date) {
				getDatepickerRange(this.id,date)
				}
			
		});
		
		// Règle pour les champs date
		$('#dd'+len+',#df'+len).each(function() {
				$(this).rules('add', {
					date: {
					}
				});
			});
		$(document).off("click", ".btn-suppr",btn_suppr);				
		$(document).on("click", ".btn-suppr",btn_suppr);					
						
	
	return true;
	}
	else return false;
}

// fonction pour filtrer les champs à afficher en fonction du profil de génération
function fieldFilter(periodicite, opened, change) {
	var bloc = $('#bloc_periodicite' +  $('#periodicite').val());
	
	$('#datelist').height(373 *  $('#periodicite').val() - 1.36 * (bloc.height() + 54)  );
	if (!change){
		$(".modif_dates").toggle();
		$("#recap_dates").toggle();
		
		;
		bloc.toggle();
	}
	if ($('#datelist fieldset .btn-suppr').length > 1 ) $('#dates_suppr').show();
	else $('#dates_suppr').hide();
	if (periodicite > 1) { // récurrentes : on pourrait aussi utiliser une classe (ex. .ponctuel et .recurrente)
							
							$('#li_situation_geo').hide();
							$('#li_ouverture').show();
							
							
						}
						else { // ponctuelles 
							
							$('#li_situation_geo').show();
							$('#li_ouverture').hide();							
						}
						
	if (opened && periodicite > 1)	{
			$('#bloc_resume').show();	
			$('#ouverture_txt').hide();
	}
	else {
		$('#bloc_resume').hide();
		if (periodicite > 1)	$('#ouverture_txt').show();
	}
	
	
	
}
	
// généèr une date au format texte (du ... au .. ou Le ... + état)	
function dates_txt(dd,df,st) {

	if (st == 'Active' || st == 0) st = '';
	else st = ' (' + st + ')';
	if (df && dd != df ) return 'Du ' + dd + ' au ' + df + st; 
	else if (dd)	return 'Le ' + dd + st; 
	else return '';
		
}

// fonction pour générer la liste des dates
function dlist_populate (datas, readonly) {
										
		$('#datelist fieldset').empty();
		can_generate = true;
		dates_supprimees = '';
		dates_statuts = '';
		date_ouverture = '';// valeurs renvoyées par l'Ajax
		date_intervalle = '';
		date_sauf = '';
		var count = 0;
		
		if (datas.dates)	{
			$.each(datas.dates, function (i, value) {
			 
		  		if (addDateFieldset(i,$.datepicker.formatDate('dd/mm/yy', new Date(value.date_debut)),$.datepicker.formatDate('dd/mm/yy', new Date(value.date_fin)),value.sous_reserve, value.reporte, value.annule,readonly)) count++;
		   
			});
			date_ouverture = datas.ouverture?datas.ouverture:'';// valeurs renvoyées par l'Ajax
			date_intervalle = datas.resume?datas.resume:'';
			date_sauf = datas.sauf?datas.sauf:'';
		}
		else $('#dates_suppr').off("click",dates_suppr);
		
		if (count > 1 ) {
			$('#dates_suppr').show();
			$('#dates_suppr').on("click",dates_suppr);
		}
		else $('#dates_suppr').hide();
		
		resume_date = date_ouverture + ' ' + date_intervalle + ' ' +  date_sauf; 
		$("#resume_date").val(resume_date);
		
			
		return count;
}
// fonction pour ajouter des zéros en début de chaîne binaire
function pad (str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}

// fonction pour confirmer la suppression des dates et mettre  à jour le résumé
var btn_suppr = function () {
	var question = "Êtes-vous sûr de vouloir supprimer cette date ?";
	confirmation(question,$(this)).then(function (obj) {
		
			can_generate = false;
			var dates = obj.parent('fieldset').children("input");
			dates_supprimees +=  ', sauf ' + dates_txt(dates[0].value,dates[1].value,0);

			resume_date = date_ouverture + ' ' + date_intervalle + ' ' + date_sauf + dates_supprimees + dates_statuts ;
			$("#resume_date").val(resume_date); 
			obj.parent('fieldset').remove();
			var suppr_rule = new RRule({
				  freq: RRule.DAILY,
				  dtstart: new Date(dates[0].value.replace(/^(\d{2})\/(\d{2})\/(\d{4})$/, '$3/$2/$1')),
				  until: new Date(dates[1].value.replace(/^(\d{2})\/(\d{2})\/(\d{4})$/, '$3/$2/$1'))
				});
			console.log(rruleSet.toString());
			rruleSet.exrule(suppr_rule);
			
			console.log(suppr_rule.all());
			console.log(rruleSet.all());
			console.log(rruleSet.toString());
		});
}

var dates_suppr = function() {
	var question = "Êtes-vous sûr de vouloir supprimer toutes les dates ?";
	confirmation(question,$(this)).then(function (obj) {
		
		dlist_populate({},0);
			
	});

}
// fonction pour générer une liste de dates
var list_generate = function(obj) {
				
						var elementId = obj.attr('id');
						resume_date = '';
						$('#resume_date').val();
						var p = elementId; // hebdo ou mensuel en fonction du bouton de génération
					
						var olddays=[0,0,0,0,0,0,0];
						var days=[];
					
						var date = $("#date_"+ p +"_debut").val();
						var startDate = $.datepicker.parseDate('dd/mm/yy', date);
						var date2 = $("#date_"+ p +"_fin").val();
						var endDate = $.datepicker.parseDate('dd/mm/yy', date2);
						var sfdd = $("#sauf_date_"+ p +"_debut").val();
						var exStartDate = $.datepicker.parseDate('dd/mm/yy', sfdd);
						var sfdf = $("#sauf_date_"+ p +"_fin").val();
						var exEndDate = $.datepicker.parseDate('dd/mm/yy', sfdf);
						var feries = $('#jf_' + p).is(':checked')?1:0; 
						var jours_coches = true;
				
						var jour = $('li.' + p + ' fieldset.jours').map(function(index) // chaque série de jours à cocher du bloc en cours
						{
							if ($('input[name="jour_' + p + (index + 1) + '"]').filter(function(){return $(this).is(':checked');}).length > 4 )	jours_coches = false;
							return $('input[name="jour_' + p + (index + 1) + '"]').map(function()
								{
									return $(this).is(':checked')?1:0;
								});
								
						});	// on obtient un tableau à  1 ou 3 dimensions (nb de blocs) dont les valeurs sont constituées de tableaux de  1 et de 0 (jour coché ou non )
					
						
						if (p == 'mensuel')	jour.each(function(index) { 
							
							var numjour_bin = parseInt($("#numero_jour_mensuel"+(index+1)).val(), 10);// valeur binaire du jour du mois correspondant sélectionné
							 
							var jours_select = jour.get(index).get(); // tableau des jours sélectionnés

							
							days = $.map( jours_select, function( val, i ) {
								newdays = (numjour_bin | olddays[i]);
							
								  if (val)	{ //si  le jour de la semaine est sélectionné
									  
									   return newdays;// je mets  à la place newdays
									
									  }
								 else return olddays[i];
								});
							 ;
							olddays = days;
						
							});
							else days =  jour.get(0).get();
				
						 var len = $('.extraDate').length + 1;
						 var days_selected = parseInt(days.join(""),10);
						 
						 var manque=[];
						 var defauts =[];
						 if (!date) manque.push('la date de début');
						 if (!date2) manque.push('la date de fin');
						 if (!jours_coches) defauts.push('vous pouvez choisir au maximum 4 jours');
						 if (!days_selected) manque.push(p == 'mensuel'?'les jours du mois ou le jour de la semaine correctement associés':'le jour de la semaine');
						 if (date && date2 && (endDate <= startDate))	defauts.push('la date de fin est égale ou antérieure à la date de début');
						 if (sfdd && sfdf && exEndDate < exStartDate)	defauts.push('la date de fin de la période exceptée est  antérieure à la date de début');
						 if (date2 && endDate < today)	defauts.push('la date de fin est dépassée');
						 $('#resume_date').val('Critères incorrects pour générer les dates : ' + (manque.length?'il manque ':'') + manque.concat(defauts).join(", "));
				 
						 if(!manque.length && !defauts.length) {
							
							if(!can_generate)	{
					
						var question = "Vous avez supprimé des dates ou modifié leur statut. En regénérant les dates, vous allez perdre ces modifications. Souhaitez-vous continuer ?";
						confirmation(question,obj).then(function (obj) {
								can_generate = true;
							
								list_generate(obj);
								
								});
							}
							else {
							
								if (p == 'hebdo') {
									
									var debut_previous_dayofweek_index = startDate.getDay();
									var debut_dayofweek_index = debut_previous_dayofweek_index + 1;
									if (debut_dayofweek_index == 7) debut_dayofweek_index = 0;
									
									var fin_dayofweek_index = endDate.getDay() + 1;
									if (fin_dayofweek_index == 7) fin_dayofweek_index = 0;
									var fin_next_dayofweek_index = fin_dayofweek_index + 1 ;
									if (fin_next_dayofweek_index == 7) fin_next_dayofweek_index = 0;
									
						
									// cas particulier si la date de début de l'intervalle est un jour compris dans une série de jours de la semaine, il y a ambigüité : ex on commence un dimanche et on a sélectionné samedi-dimanche (week-ends) : cela fausse la génération des dates qui exclut le 1er samedi - idem en fin d'intervalle)
									var msg_alert = '';
									if (days[debut_previous_dayofweek_index] && days[debut_dayofweek_index]) {
										var selected_daynames = days.map(function(val,index){if (val)	return daynames[index];	}); // chaque série de jours à cocher du bloc en cours
										
										msg_alert = '<br/>Votre période commence un ' + $.datepicker.formatDate('DD', startDate) + '. Cela crée une ambiguïté sur le premier jour de l\'intervalle.';
									}
									if (days[fin_next_dayofweek_index] && days[fin_dayofweek_index]) {
										var selected_daynames = days.map(function(val,index){if (val)	return daynames[index];	}); // chaque série de jours à cocher du bloc en cours
										 msg_alert += '<br/>Votre période se termine un ' + $.datepicker.formatDate('DD', endDate) + '. Cela crée une ambiguïté sur le dernier jour de l\'intervalle.';
									}
									if (msg_alert)		simpledialog("Vous avez sélectionné " + selected_daynames.filter(function(val){return val;}).join(',') + msg_alert+"<br/>Vérifiez les dates générées et modifiez si besoin votre période.");
								
					
								}
									
								switch (p)
								{
									case "hebdo":
										var rule = new RRule({
										  freq: RRule.WEEKLY,
										  byweekday: days.map(function(val,index){if (val) return RRule[daynames_short[index]]}).filter(function(val) {return val;	}),
										  dtstart: startDate,
										  until: endDate
										});
										if (exStartDate && exEndDate) {
											var exRule = new RRule({
											  freq: RRule.DAILY,
											  dtstart: exStartDate,
											  until: exEndDate
											});
										}
									break;
									case "mensuel":
										var rule = new RRule({
										  freq: RRule.MONTHLY,
										  byweekday: [RRule.MO, RRule.FR],
										  dtstart: startDate,
										  until: endDate
										});
									break;
								}
								rruleSet = new RRuleSet();
								rruleSet.rrule(rule);
								if (exRule) {
									rruleSet.exrule(exRule);
								}
								
					console.log(days);console.log(days.map(function(val,index){if (val) return RRule[daynames_short[index]]}).filter(function(val) {return val;	}).join(','));
					console.log(rruleSet.all());	
					console.log(rruleSet.toString());	
					if (exRule) console.log(exRule.all());
								
								$.ajax({
										type: "POST",
										url: "/ajax/ajax.genere_dates.php",
										data: {  
												 periodicite: p,
												 date_debut: date,
												 date_fin: date2,
												 jf: feries,
												 sauf_date_debut: sfdd,
												 sauf_date_fin: sfdf,
												 jours : days
												  },
									
										dataFilter:function (data){ 
												var json = JSON.parse(data);
												if (json.valid) {
													if (dlist_populate(json.datas,true) > 0 )		return true;
													else {
														simpledialog("Aucune date correspondante dans l'intervalle spécifié");
													
														return false;
													}
												}
												else {
													$("#resume_date").attr('title',json.error);
													return false;
												}
											},
										success: function(data){
											
											 can_save = true;
										
													
											}
										
										});	
							}
						
						 }
						 
						 
					//}
				}	
				
// fonction pour vouvrir une bo^^ite de dialogue demandant confirmation								
function confirmation(question,obj) {
    var defer = $.Deferred();
    $('<div></div>')
        .html(question)
        .dialog({
            autoOpen: true,
            modal: true,
            title: 'Confirmation',
            buttons: {
                "Oui": function () {
                    defer.resolve(obj);
                    $(this).dialog("close");
                },
                "Non": function () {
                    defer.reject();
                    $(this).dialog("close");
                }
            },
            close: function () {
                //$(this).remove();
				$(this).dialog('destroy').remove()
            }
        });
    return defer.promise();
}



//fonction pour contrôler la cohérence avant l'enregistrement des dates
var date_validate = function() {
					if (!$('#recap_dates').length)	 return true;//orga seul
					if(!can_save)	{
						var question = "Vous avez modifié les critères de dates sans regénérer la liste. Si vous souhaitez enregistrer les dates en l'état, répondez OUI sinon répondez NON et regénérez la liste.";
						confirmation(question,$(this)).then(function (obj) {
								can_save = true;
								date_validate();
								
								
							});
					}
					else {
						if ( $('#datelist fieldset fieldset').length > 0) {	
							var dates = [];
							if ($('#periodicite').val() == 1) { // cas des ponctuelles
								$('#datelist fieldset fieldset').each(
									function (data) {
										var date = [];
									
										$(this).children("input, select").each(
											function (data) {
												
												if ($(this).get(0).tagName == 'INPUT')	date.push($(this).val().replace(/^(\d{2})\/(\d{2})\/(\d{4})$/, '$3/$2/$1'));
												else date.push($('#' + $(this).attr('id') + ' option:selected').text());
												
										});
										
										dates.push(dates_txt($.datepicker.formatDate('D d MM yy', new Date(date[0])),$.datepicker.formatDate('D d MM yy', new Date(date[1])),date[2]));
										
								});
							
								date_intervalle = dates.join(" - ");
								date_ouverture = '';
								resume_date = date_ouverture + ' ' + date_intervalle; 
							}

							fieldFilter($('#periodicite').val(),0,0);
							$('#periodicite_txt').val($('#periodicite option:selected').text());
							$('#resume_date_txt').val(date_intervalle);
							$('#ouverture_txt').val(date_ouverture + date_sauf);
							$('#ouverture').val(date_ouverture + date_sauf);
							
							$("#resume_date").val(resume_date);
							
							$(document).off("click", ".btn-suppr",btn_suppr);
						
							
							$("#hebdo, #mensuel").on("click",function () {list_generate($(this))}); //boutons générér dates
							
						}
						else {
							simpledialog("Au minimum une date est nécessaire pour valider");
							return false;
						}
						
					}
					return true;
				}	
			
			var simpledialog = function (txt) {
				$("<div title='Avertissement'>" + txt + "</div>").dialog(
								{ 
									resizable: false,
									buttons: {
									  "OK": function () {
											$(this).dialog("close");
										  }
									 }
								}
							);
			};
				
			$('.prive').find('input:enabled:visible, select, textarea').each(function() {
					$(this).data('setval',$(this).val());
					$(this).focusout(
						function () {
							
							if($(this).val() != $(this).data('setval')) {
								$('#is_modified_organisateur').val('1');
								can_submit = true;
							}
							
						
					});
					
				});	
			$('.public').find('input:enabled:visible, select, textarea').each(function() {
					val = $(this).attr('type') == 'checkbox'?$(this).is(":checked"):$(this).val();
					$(this).data('setval',val);
					
					$(this).focusout(
						function () {
							val = $(this).attr('type') == 'checkbox'?$(this).is(":checked"):$(this).val();
							if(val != $(this).data('setval')) {
								can_submit = true;
							}
							
						
					});
					
				});			
			
					
			
			
	});